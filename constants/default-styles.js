import { StyleSheet } from 'react-native';

// here we now import into each component and reach out like this:
// import DefaultStyles from 'constants/default-styles'
// style{DefaultStyles.bodyText}

export default StyleSheet.create({
  bodyText: {
    fontFamily: 'open-sans',
    color: 'red',
  },
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
  },
});
