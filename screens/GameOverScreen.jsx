import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  Button,
  Image,
  ScrollView,
} from 'react-native';

import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import Colors from '../constants/colors';

import MainButton from '../components/MainButton';

// images you can add a prop resizeMode where you set cover contain -- will not take the box -- just shrink
// to not exceed // cover not shrink - - just crop beyond the boundaries
// cover is the default
// contain will not have any cropping

// Note Text nodes can be inside of other Text nodes
// you can nest text components
const GameOverScreen = (props) => {
  return (
    <ScrollView>
      <View style={styles.screen}>
        <TitleText>Game is Over! </TitleText>
        <Text>Game is Over!</Text>
        <View style={styles.imageContainer}>
          <Image
            // source={require('../assets/success.png')}
            source={{
              uri: 'https://cdn.pixabay.com/photo/2016/05/05/23/52/mountain-summit-1375015_960_720.jpg',
            }}
            style={styles.image}
            resizeMode="cover"
          />
        </View>
        {/* <Text numberOfLines={1} ellipsizeMode="tail">
      This text will never wrap into a new line, instead it will be cut off like this if it is too lon...
    </Text> */}
        <View style={styles.resultContainer}>
          <BodyText style={styles.resultText}>
            Your phone needed{' '}
            <Text style={styles.highlight}>{props.roundsNumber}</Text> rounds to
            guess the number{' '}
            <Text style={styles.highlight}>{props.userNumber}</Text>.
          </BodyText>
        </View>

        {/* <MainButon title="NEW GAME" onPress={props.onRestart} /> */}
        <MainButton onPress={props.onRestart}>New Game</MainButton>
      </View>
    </ScrollView>
  );
};

export default GameOverScreen;

// images are able to be managed by the image component when
// importing -- the system can figure it out
// but when coming from a URL -- you do have to set details as the system cannot figure
// always ahve to set width and height on the URL image
// styles are always applied to just the one component -- not the nested one
// they are not passed down

// however TEXT does inherit styles -- passed down to nested compoennt
// TEXT inside of TEXT is automatically recieved by nested texts
// TEXT COMPONENT does not use FLEX BOX
// text automatically wraps itself if does not fit in the screens
// TEXT PASSES STYLE DOWN -- OWN LAYOUT SYSTEM AND AUTO WRAPS IN NEW LINE
// DON'T WANT TO WRAP ? SET numberOfLines prop maybe combined with ellipsizeMode to truncate instead of wrap
const styles = StyleSheet.create({
  highlight: {
    color: Colors.primary,
    fontFamily: 'open-sans-bold',
  },
  imageContainer: {
    width: 300,
    height: 300,
    // border radius needs to be half
    //width and height need to be equal then border radius half
    borderRadius: 150,
    borderWidth: 3,
    borderColor: 'black',
    overflow: 'hidden',
    marginVertical: 30,
    //need to add overflow value hidden
    //otherwise image will not be inside continaer
    overflow: 'hidden',
    // add a little more space in the container
    marginVertical: 30,
  },
  image: {
    width: '100%',
    height: '100%',
  },
  resultContainer: {
    marginHorizontal: 30,
    marginVertical: 15,
  },
  resultText: {
    textAlign: 'center',
    fontSize: 20,
  },
  screen: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
  },
});
