import React, { useState, useRef, useEffect } from 'react';
import {
  Dimensions,
  FlatList,
  View,
  Text,
  StyleSheet,
  Button,
  Alert,
  ScrollView,
} from 'react-native';

// here is how you import an icons
// search for icons on expo vector - icons -material, ant ionicon etc
import { Ionicons } from '@expo/vector-icons';

import NumberContainer from '../components/NumberContainer';
import Card from '../components/Card';
import BodyText from '../components/BodyText';
import MainButton from '../components/MainButton';
// helps you detect screen orientation and will allow you to overrirde
// json package 'portrait' setting at runtime
import * as ScreenOrientation from 'expo-screen-orientation';

// useRef -- allows you to define a vlue which survives re-renders
// use it to lock in values
//no props or state you can make a function outisde of the component
const generateRandomBetween = (min, max, exclude) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  //math random is a number between zero and 1
  // times the difference between the maximum and minim -- how big the max number can be
  // plus minimum starts from  0 -> max
  // then put floor so that a whole number
  // generates a random number
  const rndNum = Math.floor(Math.random() * (max - min)) + min;
  if (rndNum === exclude) {
    // if guess first time -- repeat is recursion
    return generateRandomBetween(min, max, exclude);
  } else {
    return rndNum;
  }
};

// this will work for Scroll View
// const renderListItem = (value, numOfRound) => (
//   <View key={value} style={styles.listItem}>
//     <BodyText>#{numOfRound}</BodyText>
//     <BodyText>{value}</BodyText>
//   </View>
// );

// IMPLEMENTATION FOR FLATLIST -- as above
// note that the second item -- last one item data is a default value apssed in
// index -- item data also has an index
// note key is no longer required as key will be done by react native flatlist
const renderListItem = (listLength, itemData) => (
  <View style={styles.listItem}>
    <BodyText>#{listLength - itemData.index}</BodyText>
    <BodyText>{itemData.item}</BodyText>
  </View>
);

const GameScreen = (props) => {
  //orientation expo's own
  // will NOT keep rotating back to portrait -- will just always stay portrait
  ScreenOrientation.lockAsync(ScreenOrientation.OrientationLock.PORTRAIT);

  // you can set up a orientation Change listener with Screen Oreination
  // or you can get Orientaion

  // only generated the first tiem -- otherwise managed by currentGuess
  const initialGuess = generateRandomBetween(1, 100, props.userChoice);
  const [currentGuess, setCurrentGuess] = useState(initialGuess);
  const [pastGuesses, setPastGuesses] = useState([initialGuess.toString()]);

  // here we create a value which continues the length of the screen -- even reload
  // note that currentLow is an object -- current is 'current value' of object
  // note that value change does not cause the component to re-render
  const currentLow = useRef(1);
  const currentHigh = useRef(100);

  // used to listen to device width
  // DEVICE LISTENER
  const [availableDeviceWidth, setAvailableDeviceWidth] = useState(
    Dimensions.get('window').width
  );
  const [availableDeviceHeight, setAvailableDeviceHeight] = useState(
    Dimensions.get('window').height
  );

  useEffect(() => {
    const updateLayout = () => {
      setAvailableDeviceWidth(Dimensions.get('window'.width));
      setAvailableDeviceHeight(Dimensions.get('window'.height));
    };
    Dimensions.addEventListener('change', updateLayout);

    //clean up to prevent too many even listeners
    return () => {
      Dimensions.removeEventListener('change', updateLayout);
    };
  });
  //END DEVICE LISTENER

  //destructure array
  const { userChoice, onGameOver } = props;

  //first argument is a function that executes on component loading
  useEffect(() => {
    if (currentGuess === userChoice) {
      onGameOver(pastGuesses.length);
    }
    // values will cause the re-run - only re-run if depencies change
  }, [currentGuess, userChoice, onGameOver]);

  const nextGuessHandler = (direction) => {
    if (
      // if wrong hint for lower or greater - if suggest lower but is alreayd lower
      (direction === 'lower' && currentGuess < props.userChoice) ||
      (direction === 'greater' && currentGuess > props.userChoice)
    ) {
      // title, message, title, action
      Alert.alert("Don't lie!", 'You know that this is wrong...', [
        { text: 'Sorry!', style: 'cancel' },
      ]);
      // if incorrect value then we finish
      return;
    }
    // if continue -- user is not lying
    if (direction === 'lower') {
      //currentHigh - is a ref object - now we upate the ref value
      // note does not cause a re-render
      // plus 1 ensures that each computer guess will be unique for the key in scroll view
      currentHigh.current = currentGuess;
    } else {
      //now set the current -- if guess too small and the number hsould be greater
      // now set the current low to the current guess
      currentLow.current = currentGuess + 1;
    }

    // now we generate a new number based on the new boundaries
    const nextNumber = generateRandomBetween(
      currentLow.current,
      currentHigh.current,
      currentGuess
    );
    setCurrentGuess(nextNumber);
    // setRounds((curRounds) => curRounds + 1);
    // curPastGuesses is current state or latest state
    // note how the new value comes at the beginning -- I have oly seen at the end
    setPastGuesses((curPastGuesses) => [
      nextNumber.toString(),
      ...curPastGuesses,
    ]);
  };

  // you can return different JSX code
  // Like  A MEDIA QUERY
  //  if (Dimensions.get('window').height > 600) {return what you want}

  // note howe we generate the round number -- see past guess.length - index on ScrollView
  // SCROLL VIEW NEEDS FLEX 1 TO WORK ON ANDROID -- OR IT WON'T SCROLL! IF SURROUNDED BY VIEW

  // let listContainerStyles = styles.listContainer; if (Dimensions.get('window) 000 )

  let listContainerStyle = styles.listContainer;

  // if (Dimensions.get('window').width < 350) {
  //   listContainerStyle = styles.listContainerBig;
  // }

  if (availableDeviceWidth < 350) {
    // listContainerStyle = styles.listContainerBig;
  }

  if (availableDeviceHeight < 500) {
    return (
      <View style={styles.screen}>
        <Text>Opponent's Guess</Text>
        <View style={styles.controls}>
          <MainButton onPress={nextGuessHandler.bind(this, 'lower')}>
            <Ionicons name="md-remove" size={24} color="white" />
          </MainButton>

          <NumberContainer>{currentGuess}</NumberContainer>
          <MainButton onPress={nextGuessHandler.bind(this, 'greater')}>
            <Ionicons name="md-add" size={24} color="white" />
          </MainButton>

          <View style={styles.listContainer}>
            <FlatList
              contentContainerStyle={styles.list}
              data={pastGuesses}
              renderItem={renderListItem.bind(this, pastGuesses.length)}
            />
          </View>
        </View>
      </View>
    );
  }

  return (
    <View style={styles.screen}>
      <Text>Opponent's Guess</Text>
      <NumberContainer>{currentGuess}</NumberContainer>
      <Card style={styles.buttonContainer}>
        {/* <Card style={Dimensions.get('window').height > 600 ? styles.buttonContainer : styles.buttonContainerSmall}
        {/* <Button title="LOWER" onPress={nextGuessHandler.bind(this, 'lower')} /> */}
        {/* <Button
          title="GREATER"
          onPress={nextGuessHandler.bind(this, 'greater')}
        /> */}
        <MainButton onPress={nextGuessHandler.bind(this, 'lower')}>
          {/* can be in between text */}
          <Ionicons name="md-remove" size={24} color="white" />
        </MainButton>
        <MainButton onPress={nextGuessHandler.bind(this, 'greater')}>
          <Ionicons name="md-add" size={24} color="white" />
        </MainButton>
      </Card>
      <View style={styles.listContainer}>
        {/* LET'S DO A FLAT LIST ALSO  
        <ScrollView contentContainerStyle={styles.list}>
          {pastGuesses.map((guess, index) =>
            renderListItem(guess, pastGuesses.length - index)
          )}
        </ScrollView> */}

        {/* RENDER ITEM EXPECTS 2 -- FLAT LIST EXPECTS OBJECTS WHERE
        EACH OBJECT HAS A KEY  -- HERE YOU CAN USE A KEY ETRACTOR*
          <FlastList keyExtractor={(item) => item.key }  - HOWEVER KEY IS EXPETED
          TO BE A STRING TO MMAKE THE 'TO STRING' WHEN ADD THE Key
          
        */}
        <FlatList
          contentContainerStyle={styles.list}
          data={pastGuesses}
          renderItem={renderListItem.bind(this, pastGuesses.length)}
        />
      </View>
    </View>
  );
};

export default GameScreen;

//ANDROID SPECIFIC TIP BELOW FOR SCROLL VIEW
// best way to styl a ScrollView is to wrap it in a View
// if want to controll the height or width to the items
// do  not add it to the scroll view  -- just add a view where you manage

// NOTE THAT styles are styled with contentContainerStyle={styles}

// NOTE THAT flat list styles differently than scroll list and may need a different width on the list

const styles = StyleSheet.create({
  controls: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '80%',
  },
  screen: {
    flex: 1,
    padding: 10,
    // alignItems: 'center', // not required for Flat list
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    //here use in a ternary condition
    marginTop: Dimensions.get('window').height > 600 ? 20 : 10,
    width: 300,
    maxWidth: '80%',
  },
  list: {
    // flex 1 -- take all the space that you can get
    // flexGrow -- be able to grow -- keeps the other behaviour normally
    // a bit more flexible -- on a scroll view it is exactly what we need
    flexGrow: 1,
    alignItems: 'center',
    //vertically here for column -- flex-end
    justifyContent: 'flex-end',
  },
  listContainer: {
    width: Dimensions.get('window').width > 350 ? '60%' : '80%',
    flex: 1,
  },
  listItem: {
    borderColor: '#ccc',
    borderWidth: 1,
    padding: 15,
    marginVertical: 10,
    backgroundColor: 'white',
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
  },
});
