import React, { useState } from 'react';
import {
  Alert, // an API we can import
  Button,
  Dimensions, // not a component an object that allows width
  Keyboard, //keyboard is an API that we can import
  KeyboardAvoidingView,
  ScrollView,
  StyleSheet,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native';
import Card from '../components/Card';
import Input from '../components/Input';
import Colors from '../constants/colors';
import NumberContainer from '../components/NumberContainer';
import BodyText from '../components/BodyText';
import TitleText from '../components/TitleText';
import MainButton from '../components/MainButton';
import { useEffect } from 'react/cjs/react.development';

// text does not support flexbox
// note keyboard can be number-pad for no decimals
// numeric for general numbers
const StartGameScreen = (props) => {
  // all input is strin g--have to make a number
  const [enteredValue, setEnteredValue] = useState('');
  const [confirmed, setConfirmed] = useState('');
  const [selectedNumber, setselectedNumber] = useState(null);
  const [buttonWidth, setButtonWidth] = useState(
    Dimensions.get('window').width / 4
  );

  useEffect(() => {
    const updateLayout = () => {
      setButtonWidth(Dimensions.get('window').width / 4);
    };

    // DIMENSIONS LISTENER
    Dimensions.addEventListener('change', updateLayout);

    return () => {
      //clean up the old one and create a new one -- only ever 1 even listener
      Dimensions.removeEventListener('change', updateLayout);
    };
  });

  // will always return current width and height but needs to ask for it
  //HERE WE CAN ADD MANUALLY OR ADD TO USE EFFECT
  // const updateLayout = () => {
  //   setButtonWidth(Dimensions.get('window').width / 4);
  // };

  // // DIMENSIONS LISTENER
  // Dimensions.addEventListener('change');
  // END DIMENSIONSN LISTENER

  const confirmInputHandler = () => {
    //can still access as the function setConfiremd is queed and not
    //executed until the next reset cycle
    let chosenNumber = parseInt(enteredValue);

    // if empty or not a number
    if (isNaN(chosenNumber) || chosenNumber <= 0 || chosenNumber > 99) {
      // title, message, [buttons]
      Alert.alert(
        'Invalid number!',
        'Number has to be a number between 1 and 99',
        [{ text: 'Okay', style: 'destructive', onPress: resetInputHandler }]
      );
      return;
    }

    setConfirmed(true);
    setselectedNumber(chosenNumber);
    setEnteredValue('');
    Keyboard.dismiss();
  };

  const numberInputHandler = (inputText) => {
    // replace anything not 0-9 with an epty string
    setEnteredValue(inputText.replace(/[^0-9]/g, ''));
  };

  //three are qued together and batched together
  const resetInputHandler = () => {
    setEnteredValue('');
    setConfirmed(false);
  };

  //here we make global within the function so may be accessed by return below
  // here we set the value confirmedOutput to an element
  let confirmedOutput;

  if (confirmed) {
    confirmedOutput = (
      <Card style={styles.summaryContainer}>
        <Text>You selected</Text>
        <NumberContainer>{selectedNumber}</NumberContainer>
        {/* note now haveto move title to between teh two elements as now passing child via props*/}
        <MainButton onPress={() => props.onStartGame(selectedNumber)}>
          START GAME
        </MainButton>
      </Card>
    );
  }

  // KEYBOARD AVOIDING VIEW -- UNDER SCROLLVIEW BUT WRAPPING EVERYTHNIG ELSE
  // not overlay
  // bheaviour positoini in keyboard -- adds padding, changes the screen layout
  // android does best on padding --- IOS does best on behaviour = "position"
  return (
    <ScrollView>
      <KeyboardAvoidingView behaviour="position" keyboardVerticalOffset={30}>
        <TouchableWithoutFeedback
          onPress={() => {
            Keyboard.dismiss();
          }}
        >
          <View style={styles.screen}>
            <TitleText styles={styles.title}>Start a New Game!</TitleText>
            <Card style={styles.inputContainer}>
              <BodyText>Select a Number</BodyText>
              <Input
                blurOnSubmit
                autoCapitalize="none"
                autoCorrect={false}
                keyboardType="number-pad"
                maxLength={2}
                onChangeText={numberInputHandler}
                style={styles.input}
                value={enteredValue}
              />
              <View style={styles.buttonContainer}>
                <View style={styles.button}>
                  <Button
                    color={Colors.cancel}
                    title="Reset"
                    onPress={resetInputHandler}
                  />
                </View>
                {/* <View style={styles.button}> */}
                <View style={{ width: buttonWidth }}>
                  <Button
                    color={Colors.confirm}
                    title="Confirm"
                    onPress={confirmInputHandler}
                  />
                </View>
              </View>
            </Card>
            {confirmedOutput}
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export default StartGameScreen;

// will not clash as independent styling
// margin vertical = margin bottom and margin top
// padding horizontal = space left and right

// shadow commands only work on IoS
// elevation ony works on android - takes deault material UI
const styles = StyleSheet.create({
  button: {
    // width: '40%',
    // screen on Android status bar will be excluded -- only relevant in Android
    // window is better

    // Dmensiosn -- get the Font scale, width and height
    // now each button is half as wide as the device
    // ensure that our buttons always respect hte device sizie
    // how many pixels on the device unlike percentage which refer to the parent
    // width is ALWAYS the width of the screen device and never the parent
    // THIS TELLS US HOW MUCH WIDTH WE HAVE AVAILABLE

    //DIEMSNIONS IS ONLY CALCULATED WHEN APP STARTS!
    // simply runs once -- not calculated

    // you need to manage the PROPERTIES THAT CAN CHANGES DYNAMICALLY -- NEED TO BE MANAGED WITH STATE

    width: Dimensions.get('window').width / 4,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    width: '100%',
  },
  input: {
    width: 50,
    textAlign: 'center',
  },
  inputContainer: {
    width: '80%',
    alignItems: 'center',
    minWidth: 300,
    maxWidth: '95%',
    // maxWidth: '80%',
    // width: 300 /* try to use percentage with min widht*/,
  },
  screen: {
    alignItems: 'center',
    flex: 1,
    padding: 10,
  },
  summaryContainer: {
    alignItems: 'center',
    marginTop: 20,
  },
  title: {
    fontSize: 20,
    marginVertical: 10,
    fontFamily: 'open-sans-bold',
  },
});
