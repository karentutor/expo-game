import React, { useState } from 'react';
import {
  SafeAreaView, // can position content -- won't haave problems with notches always wrap topmost view often not needed with libraries with navigation as they implicitly use
  StyleSheet,
  Text,
  View,
} from 'react-native';

// prolong the loading screen until a task is done
// think ascyn
import AppLoading from 'expo-app-loading';
import * as Font from 'expo-font';

import Header from './components/Header';
import GameScreen from './screens/GameScreen';
import GameOverScreen from './screens/GameOverScreen';
import StartGameScreen from './screens/StartGameScreen';

// does not need to be recreated on every render so outside of main
// function
const fetchFonts = () => {
  //load the fonts async
  // pick a key
  // here we can name whatever we want
  // they are now globally available im the app as loaded in app
  // with
  // need a different font family for each font wegith -- not bold
  return Font.loadAsync({
    'open-sans': require('./assets/fonts/OpenSans-Regular.ttf'),
    'open-sans-bold': require('./assets/fonts/OpenSans-Bold.ttf'),
  });
};

export default function App() {
  const [userNumber, setUserNumber] = useState(null);
  const [guessRounds, setGuessRounds] = useState(0);
  const [dataLoaded, setdataLoaded] = useState(0);

  // Here we return a component if data is not ready
  // this is an expo component that we return if the data is not ready
  // to be rendered
  if (!dataLoaded) {
    // note that app loading needs a function AND a promise returning function
    return (
      // App loading is typically done to load assets
      // fonts are now globally available in the app
      <AppLoading
        startAsync={fetchFonts}
        onError={(err) => console.log(err)}
        onFinish={() => setdataLoaded(true)}
      />
    );
  }
  // get ready for new game - - reset variables
  const configureNewGameHandler = () => {
    setGuessRounds(0);
    setUserNumber(null);
  };

  //bubbles up from game screen -- number of rounds
  const gameOverHandler = (numOfRounds) => {
    setGuessRounds(numOfRounds);
  };

  const startGameHandler = (selectedNumber) => {
    setUserNumber(selectedNumber);
  };

  let content = <StartGameScreen onStartGame={startGameHandler} />;

  // for testing make GameOverScreen always start
  // content = (
  //   <GameOverScreen
  //     roundsNumber={1}
  //     userNumber={1}
  //     onRestart={configureNewGameHandler}
  //   />
  // );

  // need to reset usernumber and guess rounds in configureNewGame handler to see this screen
  if (userNumber && guessRounds <= 0) {
    content = (
      <GameScreen onGameOver={gameOverHandler} userChoice={userNumber} />
    );
  } else if (guessRounds > 0) {
    content = (
      <GameOverScreen
        roundsNumber={guessRounds}
        userNumber={userNumber}
        onRestart={configureNewGameHandler}
      />
    );
  }

  // note that style from view must be moved up to SafeAreaView
  return (
    <SafeAreaView style={styles.screen}>
      <Header title="Guess a Number" />
      {content}
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  },
});
