import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Colors from '../constants/colors';

const NumberContainer = (props) => {
  return (
    <View style={styles.container}>
      <Text style={styles.number}>{props.children}</Text>
    </View>
  );
};

export default NumberContainer;

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
    borderWidth: 2,
    borderColor: Colors.accent,
    borderRadius: 10,
    justifyContent: 'center',
    marginVertical: 10,
    padding: 10,
  },
  number: {
    color: Colors.accent,
    fontSize: 22,
    textAlign: 'center',
  },
});
