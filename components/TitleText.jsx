import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

// note that when wish to  merge iwth an incoming object
// here we take all the values (styles.title defined below) and merge them with
//whatever props are passed .. note props also needs 3 dots
// sets default but also may be overwritten
const TitleText = (props) => (
  <Text style={{ ...styles.title, ...props.style }}>{props.children}</Text>
);

export default TitleText;

const styles = StyleSheet.create({
  title: {
    fontFamily: 'open-sans-bold',
    fontSize: 18,
  },
});
