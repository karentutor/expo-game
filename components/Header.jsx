import React from 'react';
import { StyleSheet, Text, View, Platform } from 'react-native'; //Platform helps you fid out hte platform that you are running on
import Colors from '../constants/colors';

import TitleText from './TitleText';

// note we look for the correct device for styling
// leaner coe e-- base code plus dynamic styles picked on what device running on
const Header = (props) => {
  return (
    <>
      {/*here we merge styles header base and choose the correct platform for the rest*/}
      <View
        style={{
          ...styles.headerBase,
          ...Platform.select({
            ios: styles.headerIOS,
            android: styles.headerAndroid,
          }),
        }}
      >
        <TitleText style={styles.title}>{props.title}</TitleText>
      </View>
    </>
  );
};

export default Header;

const styles = StyleSheet.create({
  headerBase: {
    alignItems: 'center',
    height: 90,
    justifyContent: 'center',
    paddingTop: 36,
    width: '100%',
    // backgroundColor: Platform.OS === 'android' ? Colors.primary : 'white',
    // borderBottomColor: Platform.OS === 'ios' ? '#ccc' : 'transparent',
    // borderBottomWidth: Platform.OS === 'ios' ? 1 : 0,
  },
  headerIOS: {
    backgroundColor: Colors.primary,
    borderBottomColor: '#ccc',
    borderBottomWidth: 1,
  },
  headerAndroid: {
    backgroundColor: 'white',
    borderBottomColor: 'transparent',
    borderBottomWidth: 0,

    // backgroundColor: Platform.OS === 'android' ? Colors.primary : 'white',
    // borderBottomColor: Platform.OS === 'ios' ? '#ccc' : 'transparent',
    // borderBottomWidth: Platform.OS === 'ios' ? 1 : 0,
  },
  title: {
    color: Platform.OS === 'ios' ? Colors.primary : 'white',
  },
});
