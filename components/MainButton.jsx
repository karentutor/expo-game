import React from 'react';
// TouchableNativeFeedback-- unique to Android and gives ripple effect
import {
  StyleSheet,
  Text,
  TouchableOpacity,
  TouchableNativeFeedback,
  View,
  Platform,
} from 'react-native';
import Colors from '../constants/colors';

// We do not want to handle in the button -- but inside of the component where used
//Touchable opacity will give feedback when touched and respond when touched

//Here we just push the handler back to the root App.js
// active opacity is how responsive when button is pushed --what it looks like
const MainButton = (props) => {
  //here we use a React not REACT component
  // note no angled brackets here
  // must have a CAPITAL first letter for name
  let ButtonComponent = TouchableOpacity;

  if (Platform.OS === 'android' && Platform.Version > 21) {
    ButtonComponent = TouchableNativeFeedback;
  }
  return (
    <View style={styles.buttonContainer}>
      <ButtonComponent activeOpacity={0.6} onPress={props.onPress}>
        <View style={styles.button}>
          <Text style={styles.buttonText}>{props.children}</Text>
        </View>
      </ButtonComponent>
    </View>
  );
};

export default MainButton;

const styles = StyleSheet.create({
  button: {
    backgroundColor: Colors.primary,
    paddingVertical: 12,
    paddingHorizontal: 30,
    borderRadius: 25,
  },
  buttonContainer: {
    borderRadius: 25,
    overflow: 'hidden',
  },
  buttonText: {
    color: 'white',
    fontFamily: 'open-sans',
    fontSize: 16,
  },
});
